const APICALL = "https://api.github.com/users/"
const display = document.querySelector('.display')
const form = document.querySelector('.form-github')
const inputSearch = document.querySelector('.search')

async function dataGithub(user) {
    const reponse = await fetch(`${APICALL}${user}`)
    const data = await reponse.json()

    createCard(data)
}

function createCard(user) {
    const cardHTML = `
    <div class="card">
        <img src="${user.avatar_url}" alt="Avatar utilisateur" class="avatar">
        <h2>${user.name}</h2>
        <ul class="infos">
            <li class="followers">Followers : ${user.followers}</li>
            <li class="stars">Repositories : ${user.public_repos}</li>
            <li class="bio">Bio : ${user.bio}</li>
        </ul>
    </div>
    `
    display.innerHTML = cardHTML
}

form.addEventListener('submit', (e) => {
    e.preventDefault();

    if(inputSearch.value.length > 0){
        dataGithub(inputSearch.value);
        inputSearch.value = "";
    }
})

